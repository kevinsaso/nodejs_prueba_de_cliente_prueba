var express = require('express')
var app = express()

app.get('/api_example', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ value1: 1 }));
})

app.listen(1111)
